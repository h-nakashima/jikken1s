//
// File			linetracer.h
// Header
//
// Details		<#details#>
//	
// Project		 linetracer
// Developed with [embedXcode](http://embedXcode.weebly.com)
// 
// Author		hayato
// 				Hayato Nakashima
//
// Date			2015/06/24 10:40
// Version		<#version#>
// 
// Copyright	© hayato, 2015年
// Licence    <#license#>
//
// See			ReadMe.txt for references
//


// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(SPARK) // Spark specific
#include "application.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

#ifndef linetracer_h
#define linetracer_h

// 出力信号ピン
#define STEERING_PIN 9 //ステアリング用サーボの信号線
#define LANCER_PIN 10 //ランサー用サーボの信号線
#define RIGHT_MOTOR_PIN1 11 //右モータの入力端子1
#define RIGHT_MOTOR_PIN2 3 //右モータの入力端子2
#define LEFT_MOTOR_PIN1 5 //左モータの入力端子1
#define LEFT_MOTOR_PIN2 6 //左モータの入力端子2

// センサの入力ピン
#define INSIDE_LEFT_PIN 4 //中側の左
#define INSIDE_RIGHT_PIN 2 //中側の右
#define OUTSIDE_LEFT_PIN 7 //外側の左
#define OUTSIDE_RIGHT_PIN 8 //外側の右
#define Q_SWITCH_PIN 12 //ステアリング回転角センサ（チョロQ発射スイッチ）

#define STEERING_CENTER_DEGREE 107 //ステアリングセンター角度
#define LANCER_CENTER_DEGREE 68 //ランサーセンター角度

#endif
