//
// linetracer
//
// Description of the project
// Developed with [embedXcode](http://embedXcode.weebly.com)
//
// Author 		hayato
// 				Hayato Nakashima
//
// Date			2015/06/10 10:12
// Version		<#version#>
//
// Copyright	© hayato, 2015年
// Licence		<#license#>
//
// See         ReadMe.txt for references
//


// Core library for code-sense - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad specific
#include "Energia.h"
#elif defined(LITTLEROBOTFRIENDS) // LittleRobotFriends specific
#include "LRF.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(SPARK) || defined(PARTICLE) // Particle / Spark specific
#include "application.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(REDBEARLAB) // RedBearLab specific
#include "Arduino.h"
#elif defined(ESP8266) // ESP8266 specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

// Include application, user and local libraries
#include <Utility.h>
#include <Servo.h>
#include "linetracer.h"

// Define variables and constants
//
// Brief	Name of the LED
// Details	Each board has a LED but connected to a different pin
//
#define MOTOR_U 255 // モータ出力の上限(Upper)
#define MOTOR_L -255 // モータ出力の下限(Lower)
#define STEERING_U 20 // ステアリング角度上限
#define STEERING_L -22 // ステアリング角度下限

Servo steering;  // ステアリングオブジェクト作成
Servo lancer; //ランサーオブジェクト作成

// センサの状態
int inside_left;
int inside_right;
int outside_left;
int outside_right;
int q_switch;

int steering_degree = 0; // ステアリング角度（初期値：直進）
int target_count = 0;
int lancer_degree = 0;

void SteeringDrive(int output);
//void MotorDrive(int output);
void LancerDrive(int output);
void TargetAttack(int output);
//void ChangeDriveMode();
//int CheckChangeFlag();


//int delay_time = 0;
//int motor_speed = 255;

//int line_on_count = 0;
//int line_out_count = 0;

int lancer_delay_count = 0;
int choro_q_count = 5000;
int choro_q_state = 1;

//
// Brief	Setup
// Details	Define the pin the LED is connected to
//
// Add setup code
void setup() {
    //Serial.begin(9600); // シリアル通信開始
    //Serial.println("start"); // Start表示
    steering.attach(STEERING_PIN);  // 出力ポート指定
    lancer.attach(LANCER_PIN); //出力ポート指定

//    setPWMFrequency(3, PWM_DIV32); //244.140625Hz指定
//    setPWMFrequency(11, PWM_DIV32); //244.140625Hz指定
//    setPWMFrequency(5, PWM_DIV64); //244.140625Hz指定
//    setPWMFrequency(6, PWM_DIV64); //244.140625Hz指定
    setPWMFrequency(9, PWM_DIV8); //31250Hz指定
    setPWMFrequency(10, PWM_DIV8); //31250Hz指定
    
    
//    analogWrite(RIGHT_MOTOR_PIN1, 0);
//    analogWrite(RIGHT_MOTOR_PIN2, 0);
//    analogWrite(LEFT_MOTOR_PIN1, 0);
//    analogWrite(LEFT_MOTOR_PIN2, 0);
    pinMode(INSIDE_LEFT_PIN, INPUT);
    pinMode(INSIDE_RIGHT_PIN, INPUT);
    pinMode(OUTSIDE_LEFT_PIN, INPUT);
    pinMode(OUTSIDE_RIGHT_PIN, INPUT);
    pinMode(Q_SWITCH_PIN, INPUT);

}

//
// Brief	Loop
// Details	Blink the LED
//
// Add loop code
void loop() {
    inside_left = digitalRead(INSIDE_LEFT_PIN);
    inside_right = digitalRead(INSIDE_RIGHT_PIN);
    outside_left = digitalRead(OUTSIDE_LEFT_PIN);
    outside_right = digitalRead(OUTSIDE_RIGHT_PIN);
    q_switch = digitalRead(Q_SWITCH_PIN);

    //Serial.print("input:");
    //Serial.print(inside_left);
    //Serial.print(",");
    //Serial.print(inside_right);
    //Serial.print(",");
    //Serial.print(outside_left);
    //Serial.print(",");
    //Serial.print(outside_right);
    //Serial.print(", ");
    
    //ChangeDriveMode();
    //Serial.print("mode");
    //Serial.print(drive_mode);
    //Serial.print(", ");
    
    if(inside_left == LOW){
        if(inside_right == LOW){
            //直進
            steering_degree = 0;
        }else{
            if(steering_degree<STEERING_U){
                steering_degree = steering_degree + 2;
            }
        }
    }else{
        if(inside_right == LOW){
            if(steering_degree>STEERING_L){
                steering_degree = steering_degree - 2;
            }
        }else{
            //直進処理
            steering_degree = 0;
        }
    }
    
    
    if (outside_left == LOW && outside_right == HIGH) {
        lancer_degree = -40;
        lancer_delay_count = 0;
    }
    if (outside_right == LOW && outside_left == HIGH) {
        lancer_degree = 40;
        lancer_delay_count = 0;
    }
    
    if (choro_q_count < 3000) {
        if (choro_q_state==0) {
            lancer_degree = 90;
        }else{
            lancer_degree = -90;
        }
    }else if (choro_q_count == 3000){
        if (choro_q_state==0) {
            choro_q_state = 1;
        }else{
            choro_q_state = 0;
        }
    }else if (choro_q_count >= 10000){
        if (q_switch == HIGH) {
            choro_q_count = 0;
        }
    }
    if (choro_q_count < 10000) {
        choro_q_count++;
    }
    
    //実行部
    if (abs(lancer_degree) > 45) {
        LancerDrive(lancer_degree);
    }else{
        TargetAttack(lancer_degree);
    }
    SteeringDrive(steering_degree);
//    MotorDrive(motor_speed);
    
//    delay(10);
    //Serial.print("Delay:");
    //Serial.println(delay_time);
}


void SteeringDrive(int output){
    //Serial.print("Servo:");
    //Serial.print(output);
    //Serial.print(", ");
    
    if(output<=STEERING_U && output>=STEERING_L){
        output = STEERING_CENTER_DEGREE + output;
        steering.write(output);
    }
}

 
void LancerDrive(int output){
    //Serial.print("Lancer:");
    //Serial.print(output);
    //Serial.print(", ");
    
//    if(output<=STEERING_U && output>=STEERING_L){
        output = LANCER_CENTER_DEGREE + output;
        lancer.write(output);
//    }
}

void TargetAttack(int output){
    if (lancer_delay_count >= 0 && lancer_delay_count < 3000) {
        LancerDrive(0);
        lancer_delay_count++;
    }else if (lancer_delay_count >= 3000 && lancer_delay_count <= 100000){
        LancerDrive(output);
        lancer_delay_count++;
    }else{
        lancer_delay_count = 0;
    }
    //Serial.println(lancer_delay_count);
}

/*
void MotorDrive(int output){ // MotorDrive関数、引数(MOTOR_L〜MOTOR_U)、0:停止、正:正転、負:反転
    if (output > 0) {
        //正転
        analogWrite(RIGHT_MOTOR_PIN1, output );
        analogWrite(RIGHT_MOTOR_PIN2, 0);
        analogWrite(LEFT_MOTOR_PIN1, output );
        analogWrite(LEFT_MOTOR_PIN2, 0);
        //Serial.print("Motor:+");
        //Serial.print(output);
        //Serial.print(", ");
        
    } else if (output < 0) {
        //反転
        analogWrite(RIGHT_MOTOR_PIN1, 0);
        analogWrite(RIGHT_MOTOR_PIN2, -output );
        analogWrite(LEFT_MOTOR_PIN1, 0);
        analogWrite(LEFT_MOTOR_PIN2, -output );
        //Serial.print("Motor:");
        //Serial.print(output);
        //Serial.print(", ");
        
    } else {
        //停止
        analogWrite(RIGHT_MOTOR_PIN1, 0);
        analogWrite(RIGHT_MOTOR_PIN2, 0);
        analogWrite(LEFT_MOTOR_PIN1, 0);
        analogWrite(LEFT_MOTOR_PIN2, 0);
        //Serial.print("Motor:0");
        //Serial.print(", ");
    }
}



int CheckChangeFlag(){
    if (outside_right == HIGH && outside_left == HIGH) {
        line_on_count++;
    } else {
        line_on_count = 0;
    }
    if (line_on_count==1) {
        MotorDrive(0);
        return 1;
        line_on_count = 0;
    }
    return 0;
}
*/